import { useEffect, useState } from "react";
import queryString from "query-string";
import "./App.scss";
import Pagination from "./components/Pagination";
import PostList from "./components/PostList";
import TodoForm from "./components/TodoForm";
import TodoList from "./components/ToDoList";
import PostFilterForm from "./components/PostFilterForm";
import Clock from "./components/Clock";

function App() {
  const [todoList, setTodoList] = useState([
    { id: 2, title: "hi" },
    { id: 22, title: "hi2" },
    { id: 222, title: "hi22" },
  ]);

  const [pagination, setPagination] = useState({
    _page: 1,
    _limit: 10,
    _totalRows: 12,
  });
  const [filters, setFilters] = useState({
    _limit: 10,
    _page: 1,
    title_like: "",
  });

  const [postList, setPostList] = useState([]);
  useEffect(() => {
    try {
      async function fetchPostList() {
        const stringParams = queryString.stringify(filters);
        const urlRequest =
          "http://js-post-api.herokuapp.com/api/posts?" + stringParams;
        const res = await fetch(urlRequest);
        const resJSON = await res.json();
        console.log({ resJSON });
        const { data, pagination } = resJSON;
        setPostList(data);
        setPagination(pagination);
      }
      fetchPostList();
    } catch (error) {
      console.log("Failed to fetch", error.message);
    }
  }, [filters]);

  function findIndex(todo) {
    let result = -1;
    todoList.filter((tdo, index) => {
      if (todo.id === tdo.id) {
        result = index;
      }
    });
    return result;
  }

  function handlePageChange(newPage) {
    console.log(newPage);
    setFilters({
      ...filters,
      _page: newPage,
    });
  }

  function handleTodoList(todo) {
    const index = findIndex(todo);
    console.log(index);
    if (index !== -1) {
      const newTodoList = [...todoList];
      newTodoList.splice(index, 1);
      setTodoList(newTodoList);
    }
  }

  function handleSubmitValue(value) {
    console.log(value);
    const newTodo = {
      id: todoList.length + 2,
      ...value,
    };
    const newTodoList = [...todoList];
    newTodoList.push(newTodo);
    setTodoList(newTodoList);
  }

  function handleFilterChange(value) {
    console.log(value);
    setFilters({
      ...filters,
      _page: 1,
      title_like: value.searchTerm,
    });
  }
  const [showClock, setShowClock] = useState(true);

  return (
    <div className="app">
      {showClock && <Clock />}
      <button
        onClick={() => {
          setShowClock(false);
        }}
      >
        Hide Clock
      </button>
      {/* <PostFilterForm onSubmit={handleFilterChange} />
      <PostList posts={postList} />
      <Pagination pagination={pagination} onPageChange={handlePageChange} /> */}
      {/* <TodoForm onSubmit={handleSubmitValue} />
      <TodoList todos={todoList} onTodoClick={handleTodoList} /> */}
    </div>
  );
}

export default App;
