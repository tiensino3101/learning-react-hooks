import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

Clock.propTypes = {};

function formatDate(date) {
  const hours = `0${date.getHours()}`.slice(-2);
  const minutes = `0${date.getMinutes()}`.slice(-2);
  const seconds = `0${date.getSeconds()}`.slice(-2);

  return `${hours} : ${minutes} : ${seconds}`;
}

function Clock(props) {
  const [timeString, setTimeString] = useState("");

  useEffect(() => {
    const closeInterval = setInterval(() => {
      const date = new Date();
      const newTimeString = formatDate(date);
      setTimeString(newTimeString);
    }, 1000);
    return () => {
      clearInterval(closeInterval);
    };
  }, []);

  return <p>{timeString}</p>;
}

export default Clock;
